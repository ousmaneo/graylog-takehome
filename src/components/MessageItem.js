import React from "react";
import PropTypes from "prop-types";
import Moment from "react-moment";
import MessageRenderer from "./MessageRenderer";

function MessageItem({ timestamp, message }) {
  return (
    <dd>
      <Moment unix format="MM-DD-YYYY HH:mm">
        {timestamp}
      </Moment>
      : <MessageRenderer message={message} />
    </dd>
  );
}

MessageItem.propTypes = {
  timestamp: PropTypes.number.isRequired,
  message: PropTypes.string.isRequired
};

export default MessageItem;
