import React from "react";
import PropTypes from "prop-types";
import JsonPretty from "./JsonPretty";
import { is_url, isJson } from "../Utils";

function MessageRenderer({ message }) {
  return (
    <span>
      {isJson(message) ? (
        <JsonPretty json={JSON.parse(message)} />
      ) : is_url(message) ? (
        <a href={message}>{message}</a>
      ) : (
        message
      )}
    </span>
  );
}

MessageRenderer.propTypes = {
  message: PropTypes.string.isRequired
};

export default MessageRenderer;
