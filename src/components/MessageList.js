import React, { useState, useEffect } from "react";
import axios from "axios";
import MessageItem from "./MessageItem";
const MessageList = () => {
  const [data, setData] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios("../data/messages.json");
      setData(result.data);
    };
    fetchData();
  }, []);

  return (
    <>
      {data.map(item => (
        <MessageItem key={item.timestamp} {...item} />
      ))}
    </>
  );
};

export default MessageList;
