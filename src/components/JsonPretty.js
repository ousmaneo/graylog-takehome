import React from "react";
import PropTypes from "prop-types";

function JsonPretty({ json }) {
  return <pre>{JSON.stringify(json, null, 4)}</pre>;
}

JsonPretty.propTypes = {
  json: PropTypes.any
};

export default JsonPretty;
